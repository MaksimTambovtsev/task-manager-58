package ru.tsc.tambovtsev.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.dto.logger.EntityLogDTO;
import ru.tsc.tambovtsev.tm.logger.consumer.ConsumerApplication;

import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public final class EntityLogListener implements Consumer<EntityLogDTO> {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final JmsServer jmsServer = new JmsServer();

    @Override
    @SneakyThrows
    public void accept(final EntityLogDTO event) {
        final Class entityClass = event.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            final Annotation annotation = entityClass.getAnnotation(Table.class);
            final Table table = (Table) annotation;
            event.setTable(table.name());
        }
        final String json = objectWriter.writeValueAsString(event);
        executorService.submit(() -> jmsServer.send(json));
    }

}
