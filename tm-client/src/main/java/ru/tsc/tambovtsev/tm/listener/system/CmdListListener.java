package ru.tsc.tambovtsev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.api.model.ICommand;
import ru.tsc.tambovtsev.tm.event.ConsoleEvent;
import ru.tsc.tambovtsev.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class CmdListListener extends AbstractSystemCommandListener {

    @NotNull
    public static final String NAME = "commands";

    @NotNull
    public static final String DESCRIPTION = "Show commands list.";

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @EventListener(condition = "@cmdListListener.getName() == #event.name")
    public void handlerConsole(@NotNull final ConsoleEvent event) {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractListener> commands = getCommands();
        for (final ICommand command : commands) {
            String name = command.getName();
            if (name != null && !name.isEmpty())
                System.out.println(command.getName());
        }
    }

}
