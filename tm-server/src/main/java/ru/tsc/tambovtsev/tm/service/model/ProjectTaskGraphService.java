package ru.tsc.tambovtsev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.tambovtsev.tm.api.repository.model.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.model.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.model.IProjectTaskService;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.repository.model.ProjectGraphRepository;
import ru.tsc.tambovtsev.tm.repository.model.TaskGraphRepository;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
public class ProjectTaskGraphService implements IProjectTaskService {

    @Nullable
    @Autowired
    private IProjectRepository projectRepository;

    @Nullable
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private EntityManager entityManager;

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        Optional.ofNullable(taskId).orElseThrow(IdEmptyException::new);
        @NotNull final ITaskRepository repository = new TaskGraphRepository();
        @NotNull final IProjectRepository projectRepository = new ProjectGraphRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final Task task = repository.findById(userId, taskId);
            @Nullable final Project project = projectRepository.findById(userId, projectId);
            if (task == null) return;
            entityManager.getTransaction().begin();
            task.setProject(project);
            repository.updateById(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws AbstractException {
        Optional.ofNullable(taskId).orElseThrow(IdEmptyException::new);
        @NotNull final ITaskRepository repository = new TaskGraphRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final Task task = repository.findById(userId, taskId);
            if (task == null) return;
            entityManager.getTransaction().begin();
            task.setProject(null);
            repository.updateById(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        Optional.ofNullable(projectId).orElseThrow(IdEmptyException::new);
        @NotNull final IProjectRepository projectRepository = new ProjectGraphRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            @Nullable final Project project = projectRepository.findById(userId, projectId);
            if (project == null) return;
            entityManager.getTransaction().begin();
            projectRepository.removeCascade(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
