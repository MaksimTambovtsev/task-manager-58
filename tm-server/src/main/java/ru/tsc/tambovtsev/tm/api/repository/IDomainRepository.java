package ru.tsc.tambovtsev.tm.api.repository;

public interface IDomainRepository {

    void loadDataBackup();

    void saveDataBackup();

    void loadDataBase64();

    void saveDataBase64();

    void loadDataBinary();

    void saveDataBinary();

    void loadDataJsonFasterXml();

    void saveDataJsonFasterXml();

    void loadDataJsonJaxb();

    void saveDataJsonJaxb();

    void loadDataXmlJaxb();

    void saveDataXmlJaxb();

    void loadDataXmlFasterXml();

    void saveDataXmlFasterXml();

    void loadDataYamlFasterXml();

    void saveDataYamlFasterXml();

}
