package ru.tsc.tambovtsev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.List;

public interface IProjectRepository extends IOwnerRepository<ProjectDTO> {

    void create(@NotNull ProjectDTO project);

    void updateById(@NotNull ProjectDTO project);

    @Nullable
    ProjectDTO findById(@Nullable String userId, @Nullable String id);

    void clear(@Nullable String userId);

    long getSize(@Nullable String userId);

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    List<ProjectDTO> findAllProject();

    void removeByIdProject(@Nullable String userId, @Nullable String id);

    void removeById(@Nullable String id);

    void clearProject();

}