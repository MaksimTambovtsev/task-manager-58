package ru.tsc.tambovtsev.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @Value("#{environment['buildNumber']}")
    private String applicationVersion;

    @Value("#{environment['developer']}")
    private String authorName;

    @Value("${environment['email']:mtambovtsev@t1-consulting.ru}")
    private String authorEmail;

    @Value("${environment['password.iteration']:12345}")
    private Integer passwordIteration;

    @Value("${environment['password.secret']:356585985}")
    private String passwordSecret;

    @Value("${environment['server.port']:8080}")
    private Integer serverPort;

    @Value("${environment['session.key']:6363453453}")
    private String sessionKey;

    @Value("${environment['session.timeout']:86400}")
    private Integer sessionTimeout;

    @Value("${environment['database.username']:root}")
    private String databaseUsername;

    @Value("${environment['database.password']:sadmin}")
    private String databasePassword;

    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @Value("${environment['database.driver']:com.mysql.cj.jdbc.Driver}")
    private String databaseDriver;

    @Value("${environment['database.sql_dialect']:org.hibernate.dialect.MySQL5InnoDBDialect}")
    private String databaseSQLDialect;

    @Value("${environment['database.hbm2ddl_auto']:update}")
    private String hbm2ddlAuto;

    @Value("${environment['database.show_sql']:true}")
    private String showSql;

    @Value("${environment['database.format_sql']:true}")
    private String formatSql;

    @Value("${environment['database.second_lvl_cash']:true}")
    private String secondLvlCash;

    @Value("${environment['database.factory_class']:com.hazelcast.hibernate.HazelcastLocalCacheRegionFactory}")
    private String factoryClass;

    @Value("${environment['database.use_query_cash']:true}")
    private String useQueryCash;

    @Value("${environment['database.use_min_puts']:true}")
    private String useMinPuts;

    @Value("${environment['database.region_prefix':task-manager]}")
    private String regionPrefix;

    @Value("${environment['database.config_file_path']:hazelcast.xml}")
    private String configFilePath;

}
